<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        return view('index');
    }

    public function about() {
        return view('about');
    }

    public function contact() {
	    return view('contact');
    }

    public function download()
    {
        return response()->download('../storage/app/public/CV.pdf');
        return redirect()->action('HomeController@index');
    }
}
