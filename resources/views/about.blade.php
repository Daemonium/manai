@extends('layouts.app')

@section('content')
<div class="jumbotron bg-transparent mb-0 pb-0">
    <div class="container">

        <div class="row justify-content-center">
            <img src="/img/simple-logo.png" alt="" class="h-25">
        </div>

        <div class="row">
            <div class="col-auto">
                <div class="card bg-transparent">
                    <div class="card-body">

                        <div class="card-title">
                            <h3 class="text-white"><span> Wie</span></h3>
                        </div>
                        <div class="card-text">
                            <h4 class="text-white">
                                <ul class="fa-ul text-white">
                                    <li><span class="fa-li"><i class="fas fa-bullseye"></i></span> Web Developer </li>
                                    <li><span class="fa-li"><i class="fas fa-bullseye"></i></span> LARP&apos;er </li>
                                    <li><span class="fa-li"><i class="fas fa-bullseye"></i></span> gitarist </li>
                                    <li><span class="fa-li"><i class="fas fa-bullseye"></i></span> Server Manager </li>
                                </ul>
                            </h4>
                        </div>

                        <div class="card-title">
                            <h3 class="text-white"><span> Wat doe ik </span></h3>
                        </div>
                        <div class="card-text">
                            <h4 class="text-white">
                                <ul class="fa-ul text-white">
                                    <li><span class="fa-li"><i class="fas fa-bullseye"></i></span> Producten Maken </li>
                                    <li><span class="fa-li"><i class="fas fa-bullseye"></i></span> Muziek Maken </li>
                                    <li><span class="fa-li"><i class="fas fa-bullseye"></i></span> Discord servers onderhouden </li>
                                    <li><span class="fa-li"><i class="fas fa-bullseye"></i></span> Webservers onderhouden </li>
                                </ul>
                            </h4>
                        </div>

                    </div>
                </div>
            </div>

            <div class="col-auto">
                <div class="card bg-transparent">
                    <div class="card-body">
                        <div class="card-text">
                            <h4 class="text-white pl-5">
                                <span> Projecten waar ik trots op ben</span>
                                <div class="btn-group-vertical pt-3 pl-5" role="group" aria-label="projects">
                                    <a class="btn btn-outline-secondary text-white font-standard" href="https://gitlab.com/Daemonium/netherscapelarp" target="_blank">Netherscape.nl</a></li>
                                    <a class="btn btn-outline-secondary text-white font-standard" href="https://gitlab.com/Daemonium/tavern-wench" target="_blank">Discord bot</a></li>
                                    <a class="btn btn-outline-secondary text-white font-standard" href="https://gitlab.com/Daemonium/manai" target="_blank">Manai.nl</a></li>
                                </div>
                                </span>
                            </h4>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-auto">
                <div class="card bg-transparent">
                    <div class="card-body">

                        <div class="card-title">
                            <h3 class="text-white">Een beetje over mijzelf</h3>
                        </div>
                        <div class="card-text">
                            <p class="text-white text-justify">
                                Ik ben 29 jaar oud, en ben nu ongeveer 1,5 jaar geleden begonnen met ontwikkelen van websites.
                                Hierbij maak ik vooral gebruik van Laravel als framework en daarin pakketten zoals; Bootstrap, Fontawesome,
                                en zo nog meer afhankelijk van wat er nodig is.</br>
                                Intussen heb ik hier en daar wat freelance projecten mogen oppakken en daarvoor wat websites gebouwd,
                                momenteel is het grootste project waar ik aan bezig ben netherscape.nl.
                                Dit project is bedoeld voor een LARP evenement die ik samen met een team organiseer.
                                Naast mijn web development, ben ik ook graag bezig met kleine botjes bouwen voor discord servers,
                                muziek maken met een coverbandje, en natuurlijk larpen.
                                Wil je iets dieper werkinformatie hebben? klik dan hieronder op de download link.
                            </p>
                        </div>
                        <div class="card-footer">
                            <a class="btn btn-outline-secondary text-white" href="{{ route('download') }}">Download CV</a>
                        </div>

                    </div>
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-auto">
                <div class="card bg-transparent">
                    <div class="card-body">
                        <h4 class="display-4  card-text text-standard text-white">Neem contact op</h4>
                        <p class="card-text text-standard text-white">
                            Als je vragen heb, voel je vrij om een bericht te sturen.</br>
                            Mocht je niet gelijk een berich terug krijgen, is de kans aanwezig dat ik met een project bezig ben.</br>
                            Ik doe er wel alles aan om zo snel mogelijk te antwoorden, beloofd!
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-auto pb-5">
                <div class="card bg-transparent">
                    <div class="card-body">
                        <div class="row justify-content-center">
                            <span class="card-title" style="font-size: 3rem; color: white;"><i class="far fa-address-card"></i></span>
                        </div>
                        <p class="card-text text-standard text-white mb-0">S.R. Manaï</p>
                        <p class="card-text text-standard text-white mb-0">Professor Aalberselaan 95</p>
                        <p class="card-text text-standard text-white">3118XB Schiedam</p>
                        <p class="card-text text-standard text-white">
                            <a href="tel: +31641760132">
                                <i class="fas fa-phone-square" style="font-size: 1.5rem"></i>
                            </a>
                            - (+31)6 - 41760132
                        </p>
                        <p class="card-text text-standard text-white">
                            <a href="mailto: soufianmanai@gmail.com">
                                <i class="far fa-envelope" style="font-size: 1.5rem"></i>
                            </a>
                            - soufianmanai@gmail.com
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
