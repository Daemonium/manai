<footer class="page-footer font-small elegant-color">
    <div class="container-fluid text-center">
        <div class="row elegant-color text-white py-2">
            <div class="col">
                <div>Icons by <a href="https://fontawesome.com/start" title="Fontawesome">www.fontawesome.com</a></div>
            </div>
        </div>
        <div class="row footer-copyright text-center elegant-color-dark text-white justify-content-center">
            <p class="text-center pt-3">&copy; 2020 gemaakt door:&nbsp;SRM Development</p>
        </div>
    </div>
</footer>
