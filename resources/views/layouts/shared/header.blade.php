<nav class="navbar navbar-expand-lg navbar-dark bg-transparent">
    <a class="navbar-brand" href="{{route('home')}}"><img class="navbar-brand mh-25" src="/img/simple-logo.png" style="max-height: 3rem"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#manaiMobileToggler" aria-controls="manaiMobileToggler" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="manaiMobileToggler">
        <ul class="navbar-nav mx-auto">
            <li class="nav-item active px-2">
                <a class="nav-link btn btn-outline-dark nav-text text-standard" href="{{route('home')}}">Home</a>
            </li>
            <li class="nav-item px-2">
                <a class="nav-link btn btn-outline-dark nav-text text-standard" href="{{route('about')}}">Over mij</a>
            </li>
        </ul>
        <a class="fa fa-facebook mx-2 fa-2x" href="https://www.facebook.com/graviaLycan" target="_blank"></a>
        <a class="fa fa-twitter mx-2 fa-2x" href="https://twitter.com/GraveDaemonium" target="_blank"></a>
        <a class="fab fa-linkedin mx-2 fa-2x" href="https://www.linkedin.com/in/soufian-manai-b083a2a3/" target="_blank"></a>
    </div>
</nav>
