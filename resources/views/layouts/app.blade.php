<!DOCTYPE html>
<html lang="en">
    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=no">
        <meta name="csrf-token" content="{{csrf_token()}}">

        <link href=" {{ mix('css/app.css') }}" rel="stylesheet">
        <link href=" {{ asset('/css/general.css') }}" rel="stylesheet">

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link href="https://fonts.googleapis.com/css2?family=Dancing+Script&family=Electrolize&display=swap" rel="stylesheet">

    </head>

    <body class="flex-fill">
        <div class="container-fluid p-0 wrapper-app-banner">
            @include('layouts.shared.header')

            <main role="main" id="app">
                @yield('content')
            </main>

            @include('cookieConsent::index')
            @include('layouts.shared.footer')
        </div>

        @include('layouts.shared.scripts')

    </body>
</html>
