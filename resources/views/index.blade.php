@extends('layouts.app')

@section('content')
    <div class="container-fluid">

        <div class="row justify-content-center">
            <div class="jumbotron jumbotron-fluid bg-transparent pb-0 mb-0">
                <div class="card bg-transparent text-center">
                    <div class="card-header">
                        <h1 class="text-uppercase text-white font-weight-bold" style="font-family: 'Dancing Script', cursive;">CREATIVE</h1>
                        <h1 class="text-uppercase text-white font-weight-bold">DEVELOPER</h1>
                    </div>
                    <div class="card-body pt-0">
                        <h4 class="card-title text-white">framework to frontend</h4>
                    </div>
                    <div class="card-image">
                        <img class="card-img-bottom overflow-hidden card-img" src="/img/Soufian-2.png">
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
